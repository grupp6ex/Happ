package com.groupsex.testproject.repository;

public class Concert {

    private String dayName;
    private String dayNumber;
    private String monthName;
    private String ticketSale;
    private String placeName;

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(String dayNumber) {
        this.dayNumber = dayNumber;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public String getTicketSale() {
        return ticketSale;
    }

    public void setTicketSale(String ticketSale) {
        this.ticketSale = ticketSale;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }
}
